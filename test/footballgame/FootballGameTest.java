package footballgame;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class FootballGameTest {

    private Reporter reporter;



    @Test
    public void reporterShouldReactToGoalScoredByTeamA() {
        reporter = mock(Reporter.class);
        FootballGame footballGame = new FootballGame(reporter);

        footballGame.teamScored("A");

        verify(reporter).reactToGoal("A");
    }


    @Test
    public void shouldBeOkIfWeAddReporterLater(){
        reporter = mock(Reporter.class);
        FootballGame footballGame = new FootballGame();
        footballGame.addSpectator(reporter);
        footballGame.teamScored("A");
        verify(reporter).reactToGoal("A");
    }

    @Test
    public void shouldValidToAddMoreThanOneReporter(){
        FootballGame footballGame = new FootballGame();

        Reporter reporter1 = mock(Reporter.class);
        Reporter reporter2 = mock(Reporter.class);
        Reporter reporter3 = mock(Reporter.class);

        footballGame.addSpectator(reporter1);
        footballGame.addSpectator(reporter2);
        footballGame.addSpectator(reporter3);

        footballGame.teamScored("A");

        verify(reporter1).reactToGoal("A");
        verify(reporter2).reactToGoal("A");
        verify(reporter3).reactToGoal("A");
    }

    @Test
    public void shouldValidToAddMoreThanOneFans(){

        FootballFans fan1 = mock(FootballFans.class);
        FootballFans fan2 = mock(FootballFans.class);
        FootballFans fan3 = mock(FootballFans.class);

        FootballGame footballGame = new FootballGame(fan1);

        footballGame.addSpectator(fan2);
        footballGame.addSpectator(fan3);

        footballGame.teamScored("A");

        verify(fan1).reactToGoal("A");
        verify(fan2).reactToGoal("A");
        verify(fan3).reactToGoal("A");
    }

    @Test
    public void shouldValidAddScoreBoard(){
        Scoreboard scoreboard = mock(Scoreboard.class);

        FootballGame footballGame = new FootballGame(scoreboard);
        footballGame.teamScored("A");

        verify(scoreboard).reactToGoal("A");
    }

    // should have a test to have both FootballFans and Reporter 
    // so that we can ensure the both Spectators are being notify 
    // as part of the observer pattern
}
