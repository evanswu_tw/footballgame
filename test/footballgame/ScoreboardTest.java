package footballgame;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class ScoreboardTest {

    @Test
    public void shouldScoreboardReactToTheTeamScoresA() {
        Scoreboard scoreboard = new Scoreboard();
        assertThat(scoreboard.reactToGoal("A"), is("A-B : 1-0"));
    }

    @Test
    public void shouldScoreboardReactToTheTeamScoresB() {
        Scoreboard scoreboard = new Scoreboard();
        assertThat(scoreboard.reactToGoal("B"), is("A-B : 0-1"));
    }
}

