package footballgame;


import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

class ReporterTest {

    // its not necessary to assert for both
    // All we want to do is the make sure the reporter can take a 
    // scoring team and report accordingly
    @Test
    void shouldReportWhichTeamScored() {
        Reporter reporter = new Reporter();
        assertThat(reporter.reactToGoal("Team A"), is("GOAL by Team A"));
        assertThat(reporter.reactToGoal("Team B"), is("GOAL by Team B"));
    }
}
