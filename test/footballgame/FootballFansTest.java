package footballgame;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

class FootballFansTest {
    // one test is doing too many things here
    // it would be great if we separate this out
    // where one would be FootballFan A react positively
    // and the other test would be FootcallFan B react negatively
    @Test
    void shouldFansReactToTheTeamScores() {

        FootballFans footballFansForA = new FootballFans("A");
        FootballFans footballFansForB = new FootballFans("B");

        assertThat(footballFansForA.reactToGoal("B"), is("Boo!"));
        assertThat(footballFansForA.reactToGoal("A"), is("Yay!"));

        assertThat(footballFansForB.reactToGoal("B"), is("Yay!"));
        assertThat(footballFansForB.reactToGoal("A"), is("Boo!"));
    }
}
