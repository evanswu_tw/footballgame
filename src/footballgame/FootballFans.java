package footballgame;

public class FootballFans implements Spectator {

    private final String supportTeam;

    FootballFans(String supportTeam) {
        this.supportTeam = supportTeam;
    }

    @Override
    public String reactToGoal(String scoreTeam) {
        return supportTeam.equals(scoreTeam) ? "Yay!" : "Boo!";
    }
}
