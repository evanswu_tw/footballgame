package footballgame;

import java.util.ArrayList;

public class FootballGame {


    // when declaring type for the variable
    // its a good practise to use the interface instead of the concrete type
    // for example, in this case, it would be great if we declare spectatorArrayList
    // as List instead of ArrayList and when creating the object
    // we can define the concrete type that we want
    // which can be one of the implementation of the List - https://docs.oracle.com/javase/7/docs/api/java/util/List.html
    private ArrayList<Spectator> spectatorArrayList = new ArrayList<>();

    // do not need to define the default constructor 
    // if it is not used
    public FootballGame() {
    }

    public FootballGame(Spectator spectator) {
        if (spectator != null) {
            spectatorArrayList.add(spectator);
        }
    }

    public void teamScored(String scoringTeam) {
        if (spectatorArrayList.size() != 0) {
            for (Spectator spectator : spectatorArrayList) {
                spectator.reactToGoal(scoringTeam);
            }
        }
    }

    public void addSpectator(Spectator spectator) {
        spectatorArrayList.add(spectator);
    }
}
