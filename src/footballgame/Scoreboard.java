package footballgame;

public class Scoreboard implements Spectator {


    private int scoreForTeamA = 0;
    private int ScoreForTeamB = 0;

    @Override
    public String reactToGoal(String scoreTeam) {
        if (scoreTeam.equals("A")) {
            scoreForTeamA += 1;
        // since a football game only consist of 2 teams
        // there is no need to check if the team is teamB
        // so instead of else if, we can just do else here
        } else if (scoreTeam.equals("B")) {
            ScoreForTeamB += 1;
        }
        System.out.println(returnScore());
        return returnScore();

    }

    // this should be private method
    // to ensure low cohesion
    public String returnScore() {
        return "A-B : " + scoreForTeamA + "-" + ScoreForTeamB;
    }


}
